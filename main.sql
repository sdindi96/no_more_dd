DECLARE
   supp_type               asap.serv_req.supplement_type%TYPE;
   doc_num                 asap.serv_req.document_number%TYPE    := 7411076;
   ng_doc_num              asap.serv_req.document_number%TYPE;
   ng_ckt_cnt              NUMBER (3);
   ckt_status_inc          NUMBER (3);
   order_status            NUMBER (3);
   is_deleg_evnt_present   NUMBER (3);
   is_not_ckt_attched      NUMBER (3);
   is_deleg_tsk_present    NUMBER (3);
   ckt_id                  asap.circuit.circuit_design_id%TYPE;
   ckt_doc_num             asap.serv_req.document_number%TYPE;
   si_max_doc_num          asap.serv_req.document_number%TYPE;
   chk_doc_num             VARCHAR (10);
   ng_dd_tsk_stat          VARCHAR (10);
   lg_si_id                asap.serv_item.serv_item_id%TYPE;
   mg_ckt_id               asap.circuit.circuit_design_id%TYPE;
   mg_ckt_doc_num          asap.serv_req.document_number%TYPE;
   rev_ckt_id              asap.circuit.circuit_design_id%TYPE;
   lg_no_ckt               asap.circuit.circuit_design_id%TYPE;
BEGIN
   SELECT document_number
     INTO ng_doc_num
     FROM psr_user_data@ngmss
    WHERE legacy_order = TO_CHAR (doc_num);

   --ng dd issue
   <<ng_dd_not_cmpted>>
   SELECT task_status
     INTO ng_dd_tsk_stat
     FROM task@ngmss
    WHERE document_number = ng_doc_num AND task_type = 'DD';

   IF ng_dd_tsk_stat = 'Ready'
   THEN
      SELECT COUNT (*)
        INTO is_not_ckt_attched
        FROM serv_item@ngmss
       WHERE serv_item_id IN (
                     SELECT serv_item_id
                       FROM serv_req_si@ngmss
                      WHERE document_number = ng_doc_num
                            AND item_alias_suf = 1)
         AND circuit_design_id IS NULL;

      IF is_not_ckt_attched > 0
      THEN
         FOR b IN (SELECT *
                     FROM serv_item@ngmss
                    WHERE serv_item_id IN (
                             SELECT serv_item_id
                               FROM serv_req_si@ngmss
                              WHERE document_number = ng_doc_num
                                AND item_alias_suf = 1)
                      AND circuit_design_id IS NULL)
         LOOP
            SELECT x2.local_object_id
              INTO lg_si_id
              FROM ngmssintegration.object_xref@ngmss x1,
                   ngmssintegration.object_xref@ngmss x2
             WHERE x1.local_object_id = b.serv_item_id
               AND x1.bus_system_id = 40
               AND x1.business_entity = 'ServiceItem'
               AND x2.global_object_id = x1.global_object_id
               AND x2.bus_system_id = 18;

            SELECT NVL (circuit_design_id, 0)
              INTO mg_ckt_id
              FROM serv_item
             WHERE serv_item_id = lg_si_id;

            SELECT NVL (src.document_number, 0)
              INTO mg_ckt_doc_num
              FROM serv_item si, serv_req_si srs, service_request_circuit src
             WHERE si.serv_item_id = lg_si_id
               AND si.serv_item_id = srs.serv_item_id
               AND srs.document_number = doc_num
               AND src.circuit_design_id = si.serv_item_id;

            IF mg_ckt_id <> 0 AND mg_ckt_doc_num = 0
            THEN
               DBMS_OUTPUT.put_line
                  ('circuit is not present in service request circuit table: migration issue'
                  );
            END IF;
         END LOOP;
      ELSE
         DBMS_OUTPUT.put_line ('ng dd not completed for some issue');
      END IF;
   END IF;

   --legacy dd issue
   <<dd_already_cmpltd>>
   SELECT service_request_status
     INTO order_status
     FROM serv_req
    WHERE document_number = doc_num;

   IF order_status > 800
   THEN
      DBMS_OUTPUT.put_line ('order already completed');
   END IF;

   <<ng_ckt_deleted>>
   SELECT supplement_type
     INTO supp_type
     FROM serv_req
    WHERE document_number = doc_num;

   SELECT COUNT (*)
     INTO ng_ckt_cnt
     FROM service_request_circuit
    WHERE document_number = ng_doc_num;

   IF supp_type = 1
   THEN
      IF ng_ckt_cnt = 0
      THEN
         DBMS_OUTPUT.put_line ('change the circut status in legacy to A');
      END IF;
   ELSE
      -- circuit not in service_request_circuit(eg:patch mock ckts)
      SELECT NVL (x2.local_object_id, 0)
        INTO lg_no_ckt
        FROM service_request_circuit@ngmss nsrc,
             ngmssintegration.object_xref@ngmss x1,
             ngmssintegration.object_xref@ngmss x2,
             service_request_circuit src
       WHERE nsrc.document_number = ng_doc_num
         AND x1.local_object_id = nsrc.circuit_design_id
         AND x1.bus_system_id = 40
         AND x1.business_entity = 'Circuit'
         AND x2.global_object_id = x1.global_object_id
         AND x2.bus_system_id = 18
         AND src.circuit_design_id(+) = x2.local_object_id
         AND src.document_number(+) = doc_num
         AND src.circuit_design_id IS NULL;

      IF lg_no_ckt <> 0
      THEN
         DBMS_OUTPUT.put_line (   'circuit '
                               || lg_no_ckt
                               || ' not found in the order'
                              );
      END IF;

      -- circuit status not updated
      SELECT COUNT (*)
        INTO ckt_status_inc
        FROM service_request_circuit
       WHERE document_number = doc_num AND circuit_status IN (1, 7);

      IF ckt_status_inc > 0
      THEN
         FOR i IN (SELECT *
                     FROM service_request_circuit
                    WHERE document_number = doc_num
                      AND circuit_status IN (1, 7))
         LOOP
            SELECT x2.local_object_id
              INTO ckt_id
              FROM ngmssintegration.object_xref@ngmss x1,
                   ngmssintegration.object_xref@ngmss x2
             WHERE x1.local_object_id = i.circuit_design_id
               AND x1.bus_system_id = 18
               AND x1.business_entity = 'Circuit'
               AND x2.global_object_id = x1.global_object_id
               AND x2.bus_system_id = 40;

            SELECT COUNT (*)
              INTO ng_ckt_cnt
              FROM circuit@ngmss
             WHERE circuit_design_id = ckt_id AND status IN (4, 5, 6, 8);

            SELECT MAX (document_number)
              INTO ckt_doc_num
              FROM service_request_circuit@ngmss
             WHERE circuit_design_id = ckt_id;

            SELECT document_number
              INTO chk_doc_num
              FROM psr_user_data@ngmss
             WHERE legacy_order = TO_CHAR (doc_num);

            IF ng_ckt_cnt = 1 AND ckt_doc_num = TO_NUMBER (chk_doc_num)
            THEN
               SELECT COUNT (*)
                 INTO is_deleg_evnt_present
                 FROM serv_req_gateway_event
                WHERE document_number = 7415762
                  AND task_number IN (
                         SELECT task_number
                           FROM task
                          WHERE document_number = 7415762
                            AND task_type = 'DELEGDES');

               IF is_deleg_evnt_present = 0
               THEN
                  SELECT COUNT (*)
                    INTO is_deleg_tsk_present
                    FROM task
                   WHERE document_number = 7415762 AND task_type = 'DELEGDES';

                  IF is_deleg_tsk_present = 0
                  THEN
                     DBMS_OUTPUT.put_line ('No delegdes task event found');
                  ELSE
                     DBMS_OUTPUT.put_line ('No delegdes gateway event found');
                  END IF;
               ELSE
                  SELECT NVL (x2.local_object_id, 0)
                    INTO rev_ckt_id
                    FROM ngmssintegration.object_xref@ngmss x1,
                         ngmssintegration.object_xref@ngmss x2
                   WHERE x1.local_object_id = ckt_id
                     AND x1.bus_system_id = 40
                     AND x1.business_entity = 'Circuit'
                     AND x2.global_object_id = x1.global_object_id
                     AND x2.bus_system_id = 18;

                  IF rev_ckt_id <> i.circuit_design_id
                  THEN
                     DBMS_OUTPUT.put_line
                                    (   'incorrect reverse cross reference  '
                                     || i.circuit_design_id
                                    );
                  ELSE
                     DBMS_OUTPUT.put_line
                                      (   're-initiate delegdes for circuit '
                                       || i.circuit_design_id
                                      );
                  END IF;
               END IF;
            ELSE
               SELECT MAX (document_number)
                 INTO si_max_doc_num
                 FROM serv_req_si@ngmss
                WHERE serv_item_id IN (SELECT serv_item_id
                                         FROM serv_item@ngmss
                                        WHERE circuit_design_id = ckt_id);

               IF si_max_doc_num <> ng_doc_num
               THEN
                  DBMS_OUTPUT.put_line ('serv item rel issue');
               ELSE
                  DBMS_OUTPUT.put_line ('fail');
               END IF;
            END IF;
         END LOOP;
      END IF;
   END IF;
END;